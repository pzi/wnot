# wnot - a Notification Plugin for [WeeChat](https://weechat.org/)

--------------------------

## Introduction

wnot is a Notification Plugin for [WeeChat](https://weechat.org/).
It is specially designed for usage with the [i3bar](https://i3wm.org/i3bar/), a part of the [i3 window manager](https://i3wm.org/).
The plugin itself (wnot.so) provides POSIX shared memory containing information about the number of unread messages per buffer.
As soon as a new message arrives the buffer is adjusted.
The command line client (wnot_cli) reads all the messages from this shared memory and outputs them, depending on the configuration used.

* **--raw**: output in the format used by FIFOs previously, equal to --format "%n|%u|%j|"
* **--format <s>** print data formated by formatstring s, as known from printf
* **--i3status**: inject the notifications taken from the POSIX shared memory into the outputs of [i3status](https://i3wm.org/i3status/), such that it
can be used to display the number of unread messages in the i3bar
```
bar {
	status_command i3status | wnot_cli --i3status
	# your additional config...
}
```

## Configuration

When using *wnot_cli* in conjunction with [i3bar](https://i3wm.org/i3bar/), that is by passing the parameter *--i3status*, 
the config file *~/.i3/wnot_cli_config* is used (if existing).


When *long_output* is set to *true* every buffer containing unread messages is shown in the i3bar.
When set to *false* a summary is given ("x new messages in y buffers", can be adjusted as shown below)
```
long_output=true
```

### The long output format

When *long_output* is set to *true* a distinct entry for every buffer containing unread messages is created.

The entry's format can be adjusted using *format_chan* and *format_priv*.
As indicated by its name, *format_chan* is used as format for messages within a channel, whereas *format_priv* is used
for unread messages in a private chat.

The following conversion specifier are handled for *format_chan* and *format_priv*:

* %n - the name of the buffer
* %u - the number of unread messages
* %h - the number of highlights, "" if 0 (to reduce width of output)
* %H - the number of highlights, "0" if 0

**Both %h and %H are not used for private messages** and therefore always return "" / "0".

Apart from the format the color for each type of entry can be adjusted.
The options *color_chan_msg*, *color_chan_high* and *color_priv* can be used to set the color for messages in channel,
the color in case of a highlight (in a channel) and for private messages.

Colors are required to be in form of "#RRGGBB", just as known from HTML.
**Other formats are not supported.**

Example (values shown are the default ones):

```
format_chan="%n(%u/%h)"
format_priv="%n(%u)"

color_chan_msg="#FFFFFF"  # #FFFFFF = white
color_chan_high="#FF0000" # #FF0000 = red
color_priv="#FF0000"      # #FF0000 = red
```

### The short output format

The short output format create a single entry containing a summary of unread messages.
For this reason the option *format_short_unread* knows an extended set of conversion specifiers:

* %c - the number of buffers containing new messages
* %u - the number of unread messages (in total)
* %p - the number of private messages (in total), "" if 0
* %P - the number of private messages (in total), "0" if 0
* %h - the number of highlights (in total), "" if 0
* %H - the number of highlights (in total), "0" if 0

** *format_short_no_unread* does not accept any modifiers**, it is directly written to stdout.

The options *color_chan_msg*, *color_chan_high* and *color_priv* can be used to set the color for the whole short formatted message string.
The color is selected by the following steps:

1. have private message -> use *color_priv*
2. have highlight -> use *color_chan_high*
3. default: use *color_chan_msg*

Example (values shown are the default ones):
```
format_short_unread="%u/%h/%p messages n %c buffers"
format_short_no_unread="no unread messages"

color_chan_msg="#FFFFFF"  # #FFFFFF = white
color_chan_high="#FF0000" # #FF0000 = red
color_priv="#FF0000"      # #FF0000 = red
```

### Output in case of errors

The options *error_wnotso* and *color_error_wnotso* can be used to modify the behaviour of wnot_cli in case of a failed connection to WeeChat
(That is `WeeChat is not running` or `The wnot plugin is not loaded`).

Setting *error_wnotso* to `""` (empty string) will suppress the complete message.

Example (values shown are the default ones):
```
error_wnotso="wnot.so not loaded"
color_error_wnotso="#FF0000"
```

## Restarting *wnot_cli*

When using *wnot_cli* along with [i3bar](https://i3wm.org/i3bar/) you may not simply kill it, as
this will lead to an error message in your i3bar until you restart it (*Error: status_command process exited unexpectedly*).

For this reason *wnot_cli* restarts itself when receiving the signal *SIGUSR1*.
To send *SIGUSR1* to any running wnot_cli instance use the following command:
```
$ killall -SIGUSR1 wnot_cli
```

Reasons to restart wnot_cli:

* upgrading the client (replace executable & restart)
* modified config-file (restarting is the only way of rereading the config file)

## Premonition

wnot is currently in an early stage.
Even though it seems quite stable (I use it on a daily base), it might contain bugs.

## Building

For building the plugin only a few components are required:

* a decent C compiler (*wnot* was developed with gcc 4.9.1)
* make
* the WeeChat development headers (Debian: *weechat-dev*)
* the libConfuse (a library for parsing configuration files) development headers (Debian: *libconfuse-dev*)

For building both the plugin and the CLI client a simple *make* within the projects root folder is sufficient.

## Installing

The plugin *wnot.so* needs to be copied to the user's plugins folder (*~/.weechat/plugins/*) or to the global plugins folder (*/usr/lib/weechat/plugins/*).
Loading the plugin can be done by executing */plugin load wnot* within WeeChat.

**Unread messages which arrived prior to loading *wnot.so* are not taken into account.**

## Copyright & Licensing

wnot is Copyright (c) 2014,2015   Christian Eichler

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

