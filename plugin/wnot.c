/*
	Notification Plugin for WeeChat
	Copyright (C) 2014-2016   Christian Eichler

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include "../wnot_config.h"

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>     /* O_* */
#include <limits.h>
#include <sys/stat.h>  /* S_* */
#include <sys/types.h>
#include <sys/mman.h>
#include <errno.h>     /* errno */
#include <string.h>    /* strerror */

#include <weechat/weechat-plugin.h>

#include "callback.h"

WEECHAT_PLUGIN_NAME("wnot")
WEECHAT_PLUGIN_DESCRIPTION("Notification Plugin for WeeChat")
WEECHAT_PLUGIN_AUTHOR("Christian Eichler <code@christian-eichler.de>")
WEECHAT_PLUGIN_VERSION("0.7")
WEECHAT_PLUGIN_LICENSE("GPL3")

static struct t_weechat_plugin *weechat_plugin = NULL;
static char shm_name[NAME_MAX + 1];
static int shm_fd = -1;
static struct mq_message *shm_msgs;

int weechat_plugin_init(struct t_weechat_plugin *plugin, UNUSED(int argc), UNUSED(char **argv)) {
	weechat_plugin = plugin;

	/* generate the names of global shared memory */
	if( NAME_MAX <= snprintf(shm_name, NAME_MAX, "/wnot_shm_%d", getuid()) ) {
		weechat_log_printf("wnot: cannot assemble name of shared memory: name too long");
		return WEECHAT_RC_ERROR;
	}

	shm_fd = shm_open(shm_name, O_CREAT | O_EXCL | O_RDWR, S_IRUSR | S_IWUSR);
	if(-1 == shm_fd) {
		weechat_log_printf("wnot: cannot open shared memory: %s", strerror(errno));
		return WEECHAT_RC_ERROR;
	}

	if( -1 == ftruncate(shm_fd, SHM_MAX_STRUCT_COUNT * sizeof(struct mq_message)) ) {
		weechat_log_printf("wnot: resize the shared memory to %i Bytes: %s", SHM_MAX_STRUCT_COUNT * sizeof(struct mq_message), strerror(errno));
		return WEECHAT_RC_ERROR;
	}

	shm_msgs = mmap(NULL, SHM_MAX_STRUCT_COUNT * sizeof(struct mq_message), PROT_READ | PROT_WRITE, MAP_SHARED, shm_fd, 0);
	if( MAP_FAILED == shm_msgs ) {
		weechat_log_printf("wnot: failed to map shared memory: %s", strerror(errno));
		return WEECHAT_RC_ERROR;
	}

	callback_init(plugin, shm_msgs);

	return WEECHAT_RC_OK;
}

int weechat_plugin_end(UNUSED(struct t_weechat_plugin *plugin)) {
	callback_finalize();

	munmap(shm_msgs, SHM_MAX_STRUCT_COUNT * sizeof(struct mq_message));
	close(shm_fd);
	shm_unlink(shm_name);

	return WEECHAT_RC_OK;
}
