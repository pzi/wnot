/*
	Callbacks for the `Notification Plugin for WeeChat`
	Copyright (C) 2014-2016   Christian Eichler

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include <string.h>
#include <stdbool.h>

#include <weechat/weechat-plugin.h>

#include "../wnot_config.h"


#if APIVER >= 2016042301
	#define HOOK_SIGNAL(SIG, FUNC) weechat_hook_signal(SIG, &(FUNC), NULL, NULL)
	#define HOOK_PRINT(TAGS, FUNC) weechat_hook_print(NULL, TAGS, NULL, 0, &(FUNC), NULL, NULL)
	#define UNHOOK_ALL(PLUGIN) (PLUGIN)->unhook_all(PLUGIN, NULL)
#else
	#define HOOK_SIGNAL(SIG, FUNC) weechat_hook_signal(SIG, &(FUNC), NULL)
	#define HOOK_PRINT(TAGS, FUNC) weechat_hook_print(NULL, TAGS, NULL, 0, &(FUNC), NULL)
	#define UNHOOK_ALL(PLUGIN) (PLUGIN)->unhook_all(PLUGIN)
#endif

static struct t_weechat_plugin *weechat_plugin = NULL;
static struct t_hdata *hdata_buffer;
static struct mq_message *msg_buffer;
static int current_buffer_number;

static bool check_buffer_number(const int number) {
	if(number >= SHM_MAX_STRUCT_COUNT) {
		weechat_log_printf("wnot: shm_size too small, requested access to %ith element", number);
		return false;
	}
	return true;
}

static void update_curbuf_number(int new_number) {
	if(check_buffer_number(current_buffer_number)) {
		msg_buffer[current_buffer_number].flags &= ~FLAG_CURRENT_BUFFER;
	}

	if(check_buffer_number(new_number)) {
		msg_buffer[new_number].flags |= FLAG_CURRENT_BUFFER;
	}

	current_buffer_number = new_number;
}

/* whenever a new message is added to any buffer this function is called.
   messages to the current buffer are counted manually */
int msghook_new_message(
#if APIVER >= 2016042301
	UNUSED(const void *pointer),
#endif
	UNUSED(void *line_data), struct t_gui_buffer *buffer, UNUSED(time_t date),
	UNUSED(int tags_count), UNUSED(const char **tags), UNUSED(int displayed), int highlight,
	UNUSED(const char *prefix), UNUSED(const char *message)) {
	const int number = weechat_hdata_integer(hdata_buffer, buffer, "number");

	if(check_buffer_number(number)) {
		if(!msg_buffer[number].name[0]) {
			const char *name = weechat_hdata_string(hdata_buffer, buffer, "name");
			strncpy(msg_buffer[number].name, name, MQ_MESSAGE_MAX_NAME_LENGTH + 1);
		}

		msg_buffer[number].unread++;
		if(highlight) msg_buffer[number].high++;
	}

	return WEECHAT_RC_OK;
}


/* reset the counters for the currently visible buffer, as soon as the user interacts with weechat */ 
int sighook_reset_buffer(
#if APIVER >= 2016042301
	UNUSED(const void *pointer),
#endif
	UNUSED(void *data), UNUSED(const char *signal), UNUSED(const char *type_data), UNUSED(void *signal_data)) {

	struct t_gui_buffer *current_buffer = weechat_current_buffer();

	const int number = weechat_hdata_integer(hdata_buffer, current_buffer, "number");

	/* reset the number of unread messages in the previously focued buffer (if we just switched the buffer).
	 * we're assuming that the messages in the just left buffer have been read */
	if(number != current_buffer_number) {
		if(check_buffer_number(current_buffer_number)) {
			msg_buffer[current_buffer_number].unread = 0;
			msg_buffer[current_buffer_number].high = 0;
		}
	}

	update_curbuf_number(number);

	if(check_buffer_number(number)) {
		msg_buffer[number].unread = 0;
		msg_buffer[number].high = 0;
	}

	return WEECHAT_RC_OK;
}

int sighook_init_buffer(
#if APIVER >= 2016042301
	UNUSED(const void *pointer),
#endif
	UNUSED(void *data), UNUSED(const char *signal), UNUSED(const char *type_data), void *signal_data) {

	const int number = weechat_hdata_integer(hdata_buffer, signal_data, "number");

	if(check_buffer_number(number)) {
		msg_buffer[number].name[0] = '\0';
		msg_buffer[number].unread  = 0;
		msg_buffer[number].high    = 0;
	}

	return WEECHAT_RC_OK;
}


int sighook_remove_buffer(
#if APIVER >= 2016042301
	UNUSED(const void *pointer),
#endif
	UNUSED(void *data), UNUSED(const char *signal), UNUSED(const char *type_data), void *signal_data) {

	const int number = weechat_hdata_integer(hdata_buffer, signal_data, "number");

	if(number < SHM_MAX_STRUCT_COUNT - 1) {
		memmove(&msg_buffer[number], &msg_buffer[number + 1], (SHM_MAX_STRUCT_COUNT - 1 - number) * sizeof(struct mq_message));
	}

	return WEECHAT_RC_OK;
}

int sighook_move_buffer(
#if APIVER >= 2016042301
	UNUSED(const void *pointer),
#endif
	UNUSED(void *data), UNUSED(const char *signal), UNUSED(const char *type_data), void *signal_data) {

	const int number = weechat_hdata_integer(hdata_buffer, signal_data, "number");

	struct mq_message tmp = msg_buffer[current_buffer_number];
	if(number < current_buffer_number) {
		memmove(&msg_buffer[number + 1], &msg_buffer[number], (current_buffer_number - number) * sizeof(struct mq_message));
	} else {
		memmove(&msg_buffer[current_buffer_number], &msg_buffer[current_buffer_number + 1], (number - current_buffer_number) * sizeof(struct mq_message));
	}
	msg_buffer[number] = tmp;

	update_curbuf_number(number);

	return WEECHAT_RC_OK;
}

void callback_init(struct t_weechat_plugin *plugin, struct mq_message *_msg_buffer) {
	weechat_plugin = plugin;
	msg_buffer = _msg_buffer;

	/* get all the required hdata, saved to global (static) variables */
	hdata_buffer = weechat_hdata_get("buffer");

	update_curbuf_number(weechat_hdata_integer(hdata_buffer, weechat_current_buffer(), "number"));

	/* catch all messages, on all buffers, with color */
	if( !HOOK_PRINT(MESSAGE_TAG_FILTER, msghook_new_message)
	/* as soon as any interaction occurs we assume all messages in the current buffer have been read */
	 || !HOOK_SIGNAL("buffer_switch",           sighook_reset_buffer)
	 || !HOOK_SIGNAL("input_text_cursor_moved", sighook_reset_buffer)
	 || !HOOK_SIGNAL("input_text_changed",      sighook_reset_buffer)
	 || !HOOK_SIGNAL("buffer_opened",           sighook_init_buffer)
	 || !HOOK_SIGNAL("buffer_closed",           sighook_remove_buffer)
	 || !HOOK_SIGNAL("buffer_moved",            sighook_move_buffer) ) {
		weechat_log_printf("wnot: weechat_hook_* failed, cannot ");
		UNHOOK_ALL(plugin);
	}
}

void callback_finalize() {
	UNHOOK_ALL(weechat_plugin);
}
