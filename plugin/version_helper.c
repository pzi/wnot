/*
	Notification Plugin for WeeChat
	Copyright (C) 2016   Christian Eichler

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include <weechat/weechat-plugin.h>

int main(int argc, char *argv[]) {

	char ver[] = WEECHAT_PLUGIN_API_VERSION;

	char *end = NULL;
	uint64_t date   = strtol(ver,     &end, 10);
	uint64_t subver = strtol(&end[1], &end, 10);

	printf("-DAPIVER=%lu", date * 100 + subver);

	return EXIT_SUCCESS;
}
