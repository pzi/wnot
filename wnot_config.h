/*
	Config file for the `Notification Plugin for WeeChat`
	Copyright (C) 2014,2015   Christian Eichler

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef _WNOT_CONFIG_H
	#include <stdint.h>

	#define NDEBUG

	#define SHM_MAX_STRUCT_COUNT 64

	/*
	 * the maximum length of the buffers name in mq_message
	 */
	#define MQ_MESSAGE_MAX_NAME_LENGTH 128


	#define FLAG_CURRENT_BUFFER 1
	/*
	 * the data structure used within the POSIX shared memory for transfering 
	 * information from weechat to the client
	 */
	struct mq_message {
		char name[MQ_MESSAGE_MAX_NAME_LENGTH];
		uint16_t unread;
		uint16_t high;
		uint16_t flags;
	};

	/*
	 * PLUGIN-only configuration
	 */
	#define MESSAGE_TAG_FILTER "notify_message,notify_private,notify_highlight"

	/*
	 * CLIENT-only configuration
	 */
	#define CLIENT_I3STATUS_CFILE  "~/.i3/wnot_cli_config"


	#ifdef __GNUC__
		#define UNUSED(x) x __attribute__((__unused__))
	#else
		#define UNUSED(x) x
	#endif

	/*
	 * when NDEBUG is not defined debugging is enabled.
	 * the macro DEBUG can be used to print a message in 'format' to 'file'
	 */
	#ifndef NDEBUG
		#define DEBUG(file, format, ...) fprintf(file, "[%s:%i] " format "\n", __FILE__, __LINE__, __VA_ARGS__); fflush(file)
		#define ONLY_DEBUG(x) x
	#else
		#define DEBUG(file, format, ...)
		#define ONLY_DEBUG(x)
	#endif
#endif /* _WNOT_CONFIG_H */
