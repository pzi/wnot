# Makefile for the `Notification Plugin for WeeChat`
# Copyright (C) 2014   Christian Eichler
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

.PHONY: all install clean

green='\033[0;32m'
NC='\033[0m'

ECHO=@echo

all: wnot.so wnot_cli

wnot.so: 
	$(ECHO) -e $(green)building weechat plugin...$(NC)
	@make -C plugin all
	mv plugin/$@ .
	$(ECHO) " "

wnot_cli: 
	$(ECHO) -e $(green)building client...$(NC)
	@make -C client all
	mv client/$@ .
	$(ECHO) " "

install: wnot.so
	$(ECHO) $(green)copying weechat plugin to ~/.weechat/plugins/$(NC)
	cp wnot.so ~/.weechat/plugins/
	$(ECHO) " "

clean:
	@rm -f wnot_cli wnot.so
	@make -C plugin clean
	@make -C client clean
