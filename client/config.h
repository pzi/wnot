/*
	Configuration for the `CLI client for the Notification Plugin for WeeChat`
	Copyright (C) 2014,2015   Christian Eichler

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#ifndef _CONFIG_H
	#define _CONFIG_H

	#include <stdbool.h>

	enum config_string {FORMAT_CHAN, FORMAT_PRIV, FORMAT_SHORT_UNREAD, FORMAT_SHORT_NO_UNREAD, BUFFER_FILTER, COLOR_CHAN_MSG, COLOR_CHAN_HIGH, COLOR_PRIV, COLOR_ERROR_WNOTSO, ERROR_WNOTSO, CONFIG_STRING_SIZE};
	enum config_bool   {LONG_OUTPUT, SHOW_CHAN_MSG, SHOW_CURBUF_MSG, CONFIG_BOOL_SIZE};

	void config_init();
	char* config_get_string(enum config_string element);
	bool config_get_bool(enum config_bool element);
	void config_finalize();

#endif /* _CONFIG_H */

