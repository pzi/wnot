/*
	CLI client for the `Notification Plugin for WeeChat`
	Copyright (C) 2014,2015   Christian Eichler

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include "../wnot_config.h"

#include <stdio.h>     /* getchar, putchar */
#include <fcntl.h>     /* open */
#include <sys/stat.h>  /* open */
#include <sys/types.h> /* open */
#include <unistd.h>    /* read, write, close */
#include <string.h>    /* strlen, strtok, strchr, strcmp */
#include <limits.h>    /* MAX_NAME */
#include <stdlib.h>
#include <libgen.h>    /* basename */
#include <stdbool.h>
#include <sys/mman.h>
#include <stdint.h>
#include <signal.h>
#include <errno.h>
#include <regex.h>

#include "config.h"

ONLY_DEBUG(FILE *log_fh = NULL;)

static volatile bool restart = false;
static struct mq_message *shm_msgs = NULL;
static regex_t buffer_filter_regex;

/*
 * read chars from stdin and write them to stdout, until a char t is found.
 * t will still be written to stdout.
 *
 * returns true if t was found, false if EOF was reached prior to reaching t
 */
static bool pipe_though_until(char t) {
	int c = 0;
	do {
		c = getchar();
		if(EOF == c) return false;

		putchar(c);
	} while(t != c);

	return true;
}

/*
 * initialize the required datastructures for communication with weechat/wnot.so.
 *
 * returns true if all datastructures have been initialized,
 * false if an error occured.
 *
 * an error message will be printed if an error occurs
 */
static int shm_fd;
static bool init(bool show_errors) {
	DEBUG(log_fh, "init(%i): enter", show_errors);

	/* generate the names of global shared memory */
	char shm_name[NAME_MAX + 1];
	if( NAME_MAX <= snprintf(shm_name, NAME_MAX, "/wnot_shm_%d", getuid()) ) {
		DEBUG(log_fh, "init(%i): cannot assemble name(s) of global semaphores/message queues: name too long", show_errors);
		if(show_errors) fprintf(stderr, "wnot_cli: cannot assemble name(s) of global semaphores/message queues: name too long\n");
		return false;
	}

	shm_fd = shm_open(shm_name, O_RDONLY, S_IRUSR);
	DEBUG(log_fh, "shm_open(\"%s\", O_RDONLY, S_IRUSR) returned %i\n", shm_name, shm_fd);
	if(shm_fd < 0) {
		DEBUG(log_fh, "init(%i): cannot open shared memory: %s", show_errors, strerror(errno));
		if(show_errors) fprintf(stderr, "wnot_cli: cannot open shared memory: %s\n", strerror(errno));
		return false;
	}

	const size_t shm_size = SHM_MAX_STRUCT_COUNT * sizeof(struct mq_message);
	shm_msgs = mmap(NULL, shm_size, PROT_READ, MAP_SHARED, shm_fd, 0);
	DEBUG(log_fh, "mmap(NULL, %jd, PROT_READ, MAP_SHARED, %i, 0) returned %p\n", shm_size, shm_fd, (void*) shm_msgs);
	if(MAP_FAILED == shm_msgs) {
		DEBUG(log_fh, "init(%i): cannot mmap shared memory: %s", show_errors, strerror(errno));
		if(show_errors) fprintf(stderr, "wnot_cli: cannot mmap shared memory: %s\n", strerror(errno));
		return false;
	}

	DEBUG(log_fh, "init(%i): return true; shm_msgs = %p", show_errors, (void*) shm_msgs);
	return true;
}

static void finalize() {
	DEBUG(log_fh, "finalize(): enter%s", "");
	munmap(shm_msgs, SHM_MAX_STRUCT_COUNT * sizeof(struct mq_message));
	close(shm_fd);
	DEBUG(log_fh, "finalize(): leave%s", "");
}

/*
 * format a struct mq_message according to fmt, write to stdout
 * possible conversion specifiers:
 *  %u  the number of unread messages
 *  %h  the number of (unread) messages containing a highlight, "" if 0
 *  %H  the number of (unread) messages containing a highlight, "0" if 0
 *  %n  the buffers name
 */
static void message_format(char *fmt, struct mq_message *msg) {
	for(int i = 0; fmt[i]; i++) {
		if('%' == fmt[i]) {
			i++;
			switch(fmt[i]) {
				case 'u': printf("%d", msg->unread); break;
				case 'H': printf("%d", msg->high);   break;

				case 'n': {
					char *name = strchr(msg->name, '.') + 1;
					printf("%s", name);
					} break;

				case 'h':
					if(msg->high) printf("%d", msg->high);
					break;

			}
		} else {
			putchar(fmt[i]);
		}
	}
}

/*
 * the signal handler used to handle SIGUSR1
 */
void usr_signal_handler(UNUSED(int signum)) {
	restart = true;
}

static void i3_format_message_long(struct mq_message *shm_msgs) {
	for(int i = 0; i < SHM_MAX_STRUCT_COUNT; i++) {
		if(!shm_msgs[i].unread) continue;
		if((shm_msgs[i].flags & FLAG_CURRENT_BUFFER) && !config_get_bool(SHOW_CURBUF_MSG)) continue;
		if(!regexec(&buffer_filter_regex, shm_msgs[i].name, 0, NULL, 0)) continue;

		char *name = strchr(shm_msgs[i].name, '.') + 1;
		if('#' == *name && !shm_msgs[i].high && !config_get_bool(SHOW_CHAN_MSG)) continue;

		uid_t uid = getuid();
		printf("{\"name\":\"weechat\",\"instance\":\"%d_%s\",", uid, shm_msgs[i].name);

		if('#' == *name) {
			printf("\"color\":\"%s\",\"full_text\":\"", config_get_string(shm_msgs[i].high ? COLOR_CHAN_HIGH : COLOR_CHAN_MSG));
			message_format(config_get_string(FORMAT_CHAN), &shm_msgs[i]);
		} else {
			printf("\"color\":\"%s\",\"full_text\":\"", config_get_string(COLOR_PRIV));
			message_format(config_get_string(FORMAT_PRIV), &shm_msgs[i]);
		}
		printf("\"},");
	}
}

static void i3_format_message_short(struct mq_message *shm_msgs) {
	printf("{\"name\":\"weechat\",\"instance\":\"%d_short\",", getuid());

	uint32_t unread = 0;
	uint32_t high = 0;
	uint32_t priv = 0;
	uint32_t affected_buffers = 0;

	for(int i = 0; i < SHM_MAX_STRUCT_COUNT; i++) {
		if(!shm_msgs[i].unread) continue;
		if((shm_msgs[i].flags & FLAG_CURRENT_BUFFER) && !config_get_bool(SHOW_CURBUF_MSG)) continue;
		if(!regexec(&buffer_filter_regex, shm_msgs[i].name, 0, NULL, 0)) continue;

		char *name = strchr(shm_msgs[i].name, '.') + 1;

		unread += shm_msgs[i].unread;
		if('#' == *name) {
			high += shm_msgs[i].high;
		} else {
			priv += shm_msgs[i].unread;
		}
		affected_buffers++;
	}

	if(unread) {
		char *color = config_get_string(COLOR_CHAN_MSG);
		if(high) color = config_get_string(COLOR_CHAN_HIGH);
		if(priv) color = config_get_string(COLOR_PRIV);

		printf("\"color\":\"%s\",\"full_text\":\"", color);

		char *fmt = config_get_string(FORMAT_SHORT_UNREAD);
		int i = 0;
		while(fmt[i]) {
			if('%' == fmt[i]) {
				i++;
				switch(fmt[i]) {
					case 'u': printf("%d", unread);           break;
					case 'H': printf("%d", high);             break;
					case 'P': printf("%d", priv);             break;
					case 'c': printf("%d", affected_buffers); break;

					case 'p':
						if(priv) printf("%d", priv);
						break;
					case 'h':
						if(high) printf("%d", high);
						break;
				}
			} else {
				putchar(fmt[i]);
			}

			i++;
		}
	} else {
		printf("\"full_text\":\"%s", config_get_string(FORMAT_SHORT_NO_UNREAD));
	}
	printf("\"},");
}

static void action_i3status(bool is_restarted) {
	bool valid_shm = init(false);
	config_init();

	int regex_errno = regcomp(&buffer_filter_regex, config_get_string(BUFFER_FILTER), REG_EXTENDED | REG_NOSUB);
	if(regex_errno) {
		fprintf(stderr, "The value specified for 'buffer_filter' is not a valid POSIX.2 Extended Regular Expression\n");
		char errbuf[512] = {0};
		regerror(regex_errno, &buffer_filter_regex, errbuf, 512);
		fprintf(stderr, "Description: %s\n", errbuf);
		return;
	}

	DEBUG(log_fh, "action_i3status(): is_restarted: %i", is_restarted);

	/* even though i3status uses json, primitive string manipulation is sufficient to inject custom elements:
	    `` i3status and others will output single statuslines in one line, separated by \n. ''
	       from: http://i3wm.org/docs/i3bar-protocol.html */

	// at first just 'forward' the first two lines (version info and "[")
	if(!is_restarted) {
		if(!pipe_though_until('\n') || !pipe_though_until('\n'))
			return;
	}

	sigset_t usr_set;
	sigemptyset(&usr_set);
	sigaddset(&usr_set, SIGUSR1);

	sigprocmask(SIG_BLOCK, &usr_set, NULL);

	struct sigaction usr_action = {.sa_handler = usr_signal_handler};
	sigaction(SIGUSR1, &usr_action, NULL);

	// repeat this until i3status dies (never in the best case)
	while(true) {
		sigprocmask(SIG_UNBLOCK, &usr_set, NULL);
		if(!pipe_though_until('[')) return;
		sigprocmask(SIG_BLOCK, &usr_set, NULL);

		size_t shm_size = 0;
		if(-1 != shm_fd) {
			struct stat fbuf;
			if(-1 != fstat(shm_fd, &fbuf)) {
				valid_shm = (0 != fbuf.st_nlink);
				shm_size  = 0;
			}
		}

		if(!valid_shm) {
			DEBUG(log_fh, "shm invalid; shm_msgs = %p; reinitializing\n", (void*) shm_msgs);
			finalize();
			valid_shm = init(false);
		}

		if(valid_shm && !shm_size) {
			DEBUG(log_fh, "shm valid, but size 0; shm_msgs = %p; reinitializing\n", (void*) shm_msgs);
			finalize();
			valid_shm = init(false);
		}
		DEBUG(log_fh, "action_i3status(): shm_size: %zd", shm_size);

		if(valid_shm) {
			if(config_get_bool(LONG_OUTPUT)) {
				i3_format_message_long(shm_msgs);
			} else {
				i3_format_message_short(shm_msgs);
			}
		} else {
			DEBUG(log_fh, "action_i3status(): cannot access shm, valid_shm: %i; shm_size: %zd", valid_shm, fbuf.st_size);

			char *message = config_get_string(ERROR_WNOTSO);
			if(strcmp("", message)) {
				printf("{\"name\":\"weechat\",\"instance\":\"%d_wnot_error\",\"color\":\"%s\",\"full_text\":\"%s\"}," , getuid(), config_get_string(COLOR_ERROR_WNOTSO), message);
			}
		}

		// after injecting we need to append the remaining parts from i3status (until the next \n)
		if(!pipe_though_until('\n')) return;

		fflush(stdout);
	}

	regfree(&buffer_filter_regex);
	config_finalize();
	finalize();
}

static void action_format(char *fmt) {
	if(!init(true)) return;

	for(int i = 0; i < SHM_MAX_STRUCT_COUNT; i++) {
		if(shm_msgs[i].unread) {
			message_format(fmt, &shm_msgs[i]);
		}
	}

	finalize();
}

static void action_dump_shm() {
	if(!init(true)) return;

	for(int i = 0; i < SHM_MAX_STRUCT_COUNT; i++) {
		printf("%i: name: '%s', unread: %i, high: %i, flags: %x\n", i, shm_msgs[i].name, shm_msgs[i].unread, shm_msgs[i].high, shm_msgs[i].flags);
	}

	finalize();
}

int main(int argc, char **argv) {
	ONLY_DEBUG(log_fh = fopen("wnot_cli.log", "a");)

	if(2 <= argc) {
		/* action_i3status() should never terminate (as i3status is not intended to terminate, except on logout) */
		if(!strcmp(argv[1], "--i3status")) {
			action_i3status(argv[2] && !strcmp(argv[2], "--restart"));
			if(restart) {
				char* new_argv[argc + 2];

				memcpy(new_argv, argv, argc * sizeof(char*));
				new_argv[argc]     = "--restart";
				new_argv[argc + 1] = NULL;

				DEBUG(log_fh, "restarting wnot_cli (SIGUSR1): %s", new_argv[0]);

				execv(new_argv[0], new_argv);
			}

			ONLY_DEBUG(fclose(log_fh);)
			return EXIT_FAILURE;
		}
		else if(!strcmp(argv[1], "--format"))   action_format(argv[2]);
		else if(!strcmp(argv[1], "--raw"))      action_format("%n|%u|%h|");
		else if(!strcmp(argv[1], "--dump-shm")) action_dump_shm();
		else {
			fprintf(stderr, "Unknown parameter: %s\n", argv[1]);
			ONLY_DEBUG(fclose(log_fh);)
			return EXIT_FAILURE;
		}
	} else {
		fprintf(stderr, "Usage: %s <action> [additional parameters]\n", basename(argv[0]));
		fprintf(stderr, "\n");
		fprintf(stderr, "<action> may be one out of:\n");
		fprintf(stderr, "--i3status   inject the data read from the FIFO to the outputs of i3status\n");
		fprintf(stderr, "             (intended for usage with i3bar)\n");
		fprintf(stderr, "--format <s> print data formated by formatstring s, as known from printf\n");
		fprintf(stderr, "             possible sequences:\n");
		fprintf(stderr, "                %%n - the name of the buffer\n");
		fprintf(stderr, "                %%u - the number of unread messages\n");
		fprintf(stderr, "                %%h - the number of highlights\n");
		fprintf(stderr, "--raw        print data in the form it was send through FIFO (prior to changing to message queues)\n");
		fprintf(stderr, "             equal to '--format \"%%n|%%u|%%h|\"'\n");
		fprintf(stderr, "--dump-shm   \n");


		ONLY_DEBUG(fclose(log_fh);)
		return EXIT_FAILURE;
	}

	ONLY_DEBUG(fclose(log_fh);)
	return EXIT_SUCCESS;
}
