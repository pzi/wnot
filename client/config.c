/*
	Configuration for the `CLI client for the Notification Plugin for WeeChat`
	Copyright (C) 2014,2015   Christian Eichler

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

#include <string.h>  /* strdup */
#include <stdlib.h>  /* free   */
#include <confuse.h> /* cfg_*  */

#include "../wnot_config.h"
#include "config.h"

static char* config_strings[CONFIG_STRING_SIZE];
static bool config_bools[CONFIG_BOOL_SIZE];

void config_init() {
	config_strings[FORMAT_CHAN]            = strdup("%n(%u/%h)");
	config_strings[FORMAT_PRIV]            = strdup("%n(%u)");
	config_strings[FORMAT_SHORT_UNREAD]    = strdup("%u/%h/%p messages n %c buffers");
	config_strings[FORMAT_SHORT_NO_UNREAD] = strdup("no unread messages");
	config_strings[BUFFER_FILTER]          = strdup("weechat|server\\..*");

	config_strings[COLOR_CHAN_MSG]         = strdup("#FFFFFF");
	config_strings[COLOR_CHAN_HIGH]        = strdup("#FF0000");
	config_strings[COLOR_PRIV]             = strdup("#FF0000");

	config_strings[COLOR_ERROR_WNOTSO]     = strdup("#FF0000");
	config_strings[ERROR_WNOTSO]           = strdup("wnot.so not loaded");

	config_bools[LONG_OUTPUT]     = true;
	config_bools[SHOW_CHAN_MSG]   = false;
	config_bools[SHOW_CURBUF_MSG] = true;

	cfg_opt_t opts[] = {
		CFG_SIMPLE_STR("format_chan",            &config_strings[FORMAT_CHAN]),
		CFG_SIMPLE_STR("format_priv",            &config_strings[FORMAT_PRIV]),
		CFG_SIMPLE_STR("color_chan_msg",         &config_strings[COLOR_CHAN_MSG]),
		CFG_SIMPLE_STR("color_chan_high",        &config_strings[COLOR_CHAN_HIGH]),
		CFG_SIMPLE_STR("color_priv",             &config_strings[COLOR_PRIV]),

		CFG_SIMPLE_STR("color_error_wnotso",     &config_strings[COLOR_ERROR_WNOTSO]),
		CFG_SIMPLE_STR("error_wnotso",           &config_strings[ERROR_WNOTSO]),

		CFG_SIMPLE_STR("buffer_filter",          &config_strings[BUFFER_FILTER]),

		CFG_SIMPLE_STR("format_short_unread",    &config_strings[FORMAT_SHORT_UNREAD]),
		CFG_SIMPLE_STR("format_short_no_unread", &config_strings[FORMAT_SHORT_NO_UNREAD]),

		CFG_SIMPLE_BOOL("long_output",           &config_bools[LONG_OUTPUT]),
		CFG_SIMPLE_BOOL("show_chan_msg",         &config_bools[SHOW_CHAN_MSG]),
		CFG_SIMPLE_BOOL("show_curbuf_msg",       &config_bools[SHOW_CURBUF_MSG]),

		CFG_END()
	};

	cfg_t *cfg = cfg_init(opts, 0);
	cfg_parse(cfg, CLIENT_I3STATUS_CFILE);
	cfg_free(cfg);
}

/*
 * get the string value for a specific config option
 * the string itself is allocated internally and may not be free'd or modified
 */
char* config_get_string(enum config_string element) {
	return config_strings[element];
}

/*
 * get the boolean value for a specific config option
 */
bool config_get_bool(enum config_bool element) {
	return config_bools[element];
}

/*
 * free all internal data structures
 */
void config_finalize() {
	for(int i = 0; i < CONFIG_STRING_SIZE; i++)
		free(config_strings[i]);
}
